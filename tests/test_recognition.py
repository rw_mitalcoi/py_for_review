import unittest
import cv2
import os
from at_reception.recognition import Recognition
from pathlib import Path

base_path = Path(__file__).parent


class TestRecognition(unittest.TestCase):
    data_file = os.path.join(os.path.dirname(__file__), 'known_faces.tests.dat')

    def tearDown(self):
        if os.path.isfile(self.data_file):
            os.remove(self.data_file)

    def test_no_face_recognition(self):
        rec_module = Recognition(self.data_file)
        process_frame = cv2.imread(os.path.join(os.path.dirname(__file__), '../fixtures/mountain_1.png'))
        rec_module.recognize(process_frame)
        self.assertEqual([], rec_module.face_labels)
        self.assertEqual([], rec_module.face_locations)

    def test_success_face_recognition(self):
        rec_module = Recognition(self.data_file)
        process_frame = cv2.imread(os.path.join(os.path.dirname(__file__), '../fixtures/fabpot.jpeg'))
        rec_module.recognize(process_frame)
        self.assertEqual(['New visitor!'], rec_module.face_labels)
        self.assertEqual([(32, 80, 94, 17)], rec_module.face_locations)


if __name__ == '__main__':
    unittest.main()
