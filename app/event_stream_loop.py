import time

from flask import json
from sse import Sse

from app.video_stream_loop import VideoStreamLoop


def event_stream(vsl: VideoStreamLoop, logging):
    sse = Sse()
    while True:
        if vsl.recognized:
            logging.info(json.dumps(vsl.recognized))
            sse.add_message('recognized', json.dumps(vsl.recognized))
            yield sse.__str__()
            time.sleep(1)
