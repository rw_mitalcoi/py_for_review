from app.web import app
from flask_injector import FlaskInjector, request
from at_reception.recognition import Recognition
from at_reception.camera import VideoCamera
from app.video_stream_loop import VideoStreamLoop
import os
from logging.config import dictConfig

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

_camera = VideoCamera(int(os.getenv('VIDEO_CAPTURE_INDEX', 0)))
_rec = Recognition("known_faces.dat", int(os.getenv('VIDEO_MATRIX_DIVIDER_K', 4)))


def configure(binder):
    binder.bind(
        VideoCamera,
        to=_camera,
        scope=request,
    )
    binder.bind(
        Recognition,
        to=_rec,
        scope=request,
    )
    binder.bind(
        VideoStreamLoop,
        to=VideoStreamLoop(_camera, _rec, os.path.join(os.path.dirname(__file__), 'public/img/')),
        scope=request,
    )


FlaskInjector(app=app, modules=[configure])

if __name__ == '__main__':
    app.run(host='127.0.0.1', debug=True)
