# Always prefer setuptools over distutils
from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='AtReception',  # Required

    version='0.0.0',  # Required

    description='AtReception face at_reception module',  # Optional

    packages=find_packages(where='.'),  # Required

    python_requires='>=3.4, <4',

    install_requires=['face_recognition','dlib', 'python-dotenv', 'opencv-contrib-python-headless', 'flask'],

)
