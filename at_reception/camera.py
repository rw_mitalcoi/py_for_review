import cv2

import platform


def running_on_jetson_nano():
    # To make the same code work on a laptop or on a Jetson Nano, we'll detect when we are running on the Nano
    # so that we can access the camera correctly in that case.
    # On a normal Intel laptop, platform.machine() will be "x86_64" instead of "aarch64"
    return platform.machine() == "aarch64"


def get_jetson_gstreamer_source(capture_width=1280, capture_height=720, display_width=1280, display_height=720,
                                framerate=60, flip_method=0):
    """
    Return an OpenCV-compatible video source description that uses gstreamer to capture video from the camera on a Jetson Nano
    """
    return (
            f'nvarguscamerasrc ! video/x-raw(memory:NVMM), ' +
            f'width=(int){capture_width}, height=(int){capture_height}, ' +
            f'format=(string)NV12, framerate=(fraction){framerate}/1 ! ' +
            f'nvvidconv flip-method={flip_method} ! ' +
            f'video/x-raw, width=(int){display_width}, height=(int){display_height}, format=(string)BGRx ! ' +
            'videoconvert ! video/x-raw, format=(string)BGR ! appsink'
    )


class VideoCamera(object):

    def __init__(self, default_video_capture_code):
        self._default_video_capture_code = default_video_capture_code
        self._video_capture = None
        self._grabbed = None
        self.frame = None

    def __del__(self):
        self.release()

    def release(self) -> None:
        if self._video_capture is not None:
            self._video_capture.release()
        self.frame = None
        
    def start(self) -> None:
        if running_on_jetson_nano():
            self._video_capture = cv2.VideoCapture(get_jetson_gstreamer_source(), cv2.CAP_GSTREAMER)
        else:
            self._video_capture = cv2.VideoCapture(self._default_video_capture_code)
        (self._grabbed, self.frame) = self._video_capture.read()
