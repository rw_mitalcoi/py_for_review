import uuid
from app.video_stream_loop import VideoStreamLoop
from unittest.mock import patch

from flask_injector import FlaskInjector, request
from at_reception.camera import VideoCamera
from at_reception.recognition import Recognition
from app.web import app
import unittest
import os

TEST_UUIDS_COUNT = 0


def mock_uuid():
    global TEST_UUIDS_COUNT
    TEST_UUIDS_COUNT += 1
    return uuid.UUID(int=TEST_UUIDS_COUNT)


class TestWeb(unittest.TestCase):
    _vsl = None

    def setUp(self):
        _camera = VideoCamera(os.path.join(os.path.dirname(__file__), '../fixtures/fabpot.jpeg'))
        _rec = Recognition(os.path.join(os.path.dirname(__file__), "known_faces.tests.dat"))
        self._vsl = VideoStreamLoop(_camera, _rec, os.path.join(os.path.dirname(__file__), '../var/'))

        def configure_to_test(binder):
            binder.bind(
                VideoCamera,
                to=_camera,
                scope=request,
            )
            binder.bind(
                Recognition,
                to=_rec,
                scope=request,
            )
            binder.bind(
                VideoStreamLoop,
                to=self._vsl,
                scope=request,
            )
        FlaskInjector(app=app, modules=[configure_to_test])

        self.app = app.test_client()

    def test_stream_sse(self):
        self._vsl.recognized = {'face_labels': 'New visitor!'}
        response = self.app.get('/stream_sse')
        assert response.status == '200 OK'

    @patch('uuid.uuid4', mock_uuid)
    def test_video_feed(self):
        response = self.app.get('/video_feed')
        assert response.status == '200 OK'
        assert response.headers['Content-Type'] == 'multipart/x-mixed-replace; boundary=frame'
        assert self._vsl is not None
        assert self._vsl.recognized is not None
        assert self._vsl.recognized['face_labels'] == ['New visitor!']
        assert self._vsl.recognized['last_frame'] == '/img/00000000-0000-0000-0000-000000000001.jpg'


if __name__ == '__main__':
    unittest.main()