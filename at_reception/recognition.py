import face_recognition
from datetime import datetime, timedelta
import numpy as np
import pickle
import cv2
from typing import List
from mypy_extensions import TypedDict


class KnownFaceEncoding(TypedDict):
    first_seen: int


class Recognition(object):
    number_of_faces_since_save = 0

    def __init__(self, db, video_matrix_divider_k=4):
        self.db = db
        self.load_known_faces()
        self.known_face_encodings: List[KnownFaceEncoding] = []
        self.known_face_metadata: List = []
        self.face_labels: List[str] = []
        self.face_locations = ()
        self.number_of_faces_since_save: int = 0
        self.video_matrix_divider_k = video_matrix_divider_k

    def save_known_faces(self):
        with open(self.db, "wb") as face_data_file:
            face_data = [self.known_face_encodings, self.known_face_metadata]
            pickle.dump(face_data, face_data_file)
            print("Known faces backed up to disk.")

    def load_known_faces(self):
        try:
            with open(self.db, "rb") as face_data_file:
                self.known_face_encodings, self.known_face_metadata = pickle.load(face_data_file)
                print("Known faces loaded from disk.")
        except FileNotFoundError as e:
            print("No previous face data found - starting with a blank known face list.")
            pass

    def register_new_face(self, face_encoding, face_image):
        """
        Add a new person to our list of known faces
        """
        # Add the face encoding to the list of known faces
        self.known_face_encodings.append(face_encoding)
        # Add a matching dictionary entry to our metadata list.
        # We can use this to keep track of how many times a person has visited, when we last saw them, etc.
        self.known_face_metadata.append({
            "first_seen": datetime.now(),
            "first_seen_this_interaction": datetime.now(),
            "last_seen": datetime.now(),
            "seen_count": 1,
            "seen_frames": 1,
            "face_image": face_image,
        })

    def lookup_known_face(self, face_encoding):
        """
        See if this is a face we already have in our face list
        """
        f_metadata = None

        # If our known face list is empty, just return nothing since we can't possibly have seen this face.
        if len(self.known_face_encodings) == 0:
            return f_metadata

        # Calculate the face distance between the unknown face and every face on in our known face list
        # This will return a floating point number between 0.0 and 1.0 for each known face. The smaller the number,
        # the more similar that face was to the unknown face.
        face_distances = face_recognition.face_distance(self.known_face_encodings, face_encoding)

        # Get the known face that had the lowest distance (i.e. most similar) from the unknown face.
        best_match_index = np.argmin(face_distances)

        # If the face with the lowest distance had a distance under 0.6, we consider it a face match.
        # 0.6 comes from how the face src model was trained. It was trained to make sure pictures
        # of the same person always were less than 0.6 away from each other.
        # Here, we are loosening the threshold a little bit to 0.65 because it is unlikely that two very similar
        # people will come up to the door at the same time.
        if face_distances[best_match_index] < 0.8:
            # If we have a match, look up the metadata we've saved for it (like the first time we saw it, etc)
            f_metadata = self.known_face_metadata[best_match_index]

            # Update the metadata for the face so we can keep track of how recently we have seen this face.
            f_metadata["last_seen"] = datetime.now()
            f_metadata["seen_frames"] += 1

            # We'll also keep a total "seen count" that tracks how many times this person has come to the door.
            # But we can say that if we have seen this person within the last 5 minutes, it is still the same
            # visit, not a new visit. But if they go away for awhile and come back, that is a new visit.
            if datetime.now() - f_metadata["first_seen_this_interaction"] > timedelta(minutes=5):
                f_metadata["first_seen_this_interaction"] = datetime.now()
                f_metadata["seen_count"] += 1

        return f_metadata

    def recognize(self, process_frame) -> None:
        small_frame = cv2.resize(
            process_frame, (0, 0), fx=1 / self.video_matrix_divider_k, fy=1 / self.video_matrix_divider_k
        )

        # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
        rgb_small_frame = small_frame[:, :, ::-1]
        # Find all the face locations and face encodings in the current frame of video
        self.face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, self.face_locations)
        self.face_labels = []
        for face_location, face_encoding in zip(self.face_locations, face_encodings):
            # See if this face is in our list of known faces.
            f_metadata = self.lookup_known_face(face_encoding)

            # If we found the face, label the face with some useful information.
            if f_metadata is not None:
                time_at_door = datetime.now() - f_metadata['first_seen_this_interaction']
                self.face_labels.append(f"At reception {int(time_at_door.total_seconds())}s")

            # If this is a brand new face, add it to our list of known faces
            else:
                self.face_labels.append("New visitor!")

                # Grab the image of the the face from the current frame of video
                top, right, bottom, left = face_location
                face_image = small_frame[top:bottom, left:right]
                face_image = cv2.resize(face_image, (150, 150))

                # Add the new face to our known face data
                self.register_new_face(face_encoding, face_image)

        if len(self.face_locations) > 0 and self.number_of_faces_since_save > 100:
            self.save_known_faces()
            self.number_of_faces_since_save = 0
        else:
            self.number_of_faces_since_save += 1
