import time
from flask import Flask, render_template, Response, json
import logging

from sse import Sse

from app.event_stream_loop import event_stream
from app.video_stream_loop import VideoStreamLoop

app = Flask(__name__, template_folder="../templates")


@app.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    return response


@app.route('/img/<path>')
def root(path):
    return app.send_static_file('/img/' + path)


@app.route('/dev')
def index():
    return render_template('dev.html')


@app.route('/video_feed')
def video_feed(vsl: VideoStreamLoop):
    return Response(vsl.start(), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/stream_sse')
def stream_sse(vsl: VideoStreamLoop):
    return Response(event_stream(vsl, logging), mimetype="text/event-stream")

