import uuid
from io import BytesIO

import numpy as np
from PIL import Image

from at_reception.recognition import Recognition
from at_reception.camera import VideoCamera
import cv2
import os
from datetime import datetime, timedelta
from array import array


def get_box_by_frame(frame, rec_module: Recognition):
    for (top, right, bottom, left), face_label in zip(rec_module.face_locations, rec_module.face_labels):
        top *= int(os.getenv('VIDEO_MATRIX_DIVIDER_K', 4))
        right *= int(os.getenv('VIDEO_MATRIX_DIVIDER_K', 4))
        bottom *= int(os.getenv('VIDEO_MATRIX_DIVIDER_K', 4))
        left *= int(os.getenv('VIDEO_MATRIX_DIVIDER_K', 4))
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        cv2.putText(frame, face_label, (left + 6, bottom - 6), cv2.FONT_HERSHEY_DUPLEX, 0.8, (255, 255, 255), 1)

    number_of_recent_visitors = 0
    for metadata in rec_module.known_face_metadata:
        if datetime.now() - metadata["last_seen"] < timedelta(seconds=10) and metadata["seen_frames"] > 5:

            # Draw the known face image
            x_position = number_of_recent_visitors * 150
            frame[30:180, x_position:x_position + 150] = metadata["face_image"]
            number_of_recent_visitors += 1

            # Label the image with how many times they have visited
            visits = metadata['seen_count']
            visit_label = f"{visits} visits"
            if visits == 1:
                visit_label = "First visit"
            cv2.putText(frame, visit_label, (x_position + 10, 170), cv2.FONT_HERSHEY_DUPLEX, 0.6,
                        (255, 255, 255), 1)

    if number_of_recent_visitors > 0:
        cv2.putText(frame, "Visitors at Door", (24, 36), cv2.FONT_HERSHEY_DUPLEX, 0.8, (255, 255, 255), 1)

    (flag, encoded_image) = cv2.imencode(".jpg", frame)
    return encoded_image


class VideoStreamLoop(object):

    def __init__(self, camera: VideoCamera, rec_module: Recognition, persist_path: str):
        self.recognized = None
        self.last_frame = None
        self._persist_path = persist_path
        self._camera = camera
        self._rec_module = rec_module

    def start(self):
        while True:
            self._camera.start()
            self._rec_module.recognize(self._camera.frame[:])

            encoded_image = get_box_by_frame(self._camera.frame, self._rec_module)
            if len(self._rec_module.face_labels) > 0:
                filename = '{0}{1}'.format(uuid.uuid4(), '.jpg')
                (flag, encoded_original_image) = cv2.imencode(".jpg", self._camera.frame)
                image = Image.open(BytesIO(bytearray(encoded_original_image)))
                image.save(self._persist_path+filename)
                self.recognized = {
                    #'known_face_metadata': self._rec_module.known_face_metadata,
                    'face_labels': self._rec_module.face_labels,
                    'last_frame': '/img/'+filename,
                }
                yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' +
                       bytearray(encoded_image) + b'\r\n')
                self.release()
                break

            yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' +
                   bytearray(encoded_image) + b'\r\n')

    def release(self):
        self._camera.release()
        self.recognized = None
        cv2.destroyAllWindows()
